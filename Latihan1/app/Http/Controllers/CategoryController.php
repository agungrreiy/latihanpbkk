<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index(){
        $data = Category::orderBy('id','desc')->get();
        return view('category.index',['data' => $data]);
    }

    public function create(){
        return view('category.create');
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255'
        ]);

        if($validator->fails()){
            return redirect('/category/add')
            ->withErrors($validator)
            ->withInput();
        }else{
            $category = New Category();
            $category->name = $request->name;
            $category->save();
            return redirect('/category');
        }
    }

    public function delete($id){
        $category = Category::where('id',$id)->delete();
        return redirect('/category');
    }

    public function edit($id){
        $category = Category::where('id',$id)->first();
        return view('category.edit',['category' => $category]);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
        ]);

        if($validator->fails()){
            return redirect('/category/edit/'.$request->id)
            ->withErrors($validator)
            ->withInput();
        }else{
            $file = $request->foto;
            $category = Category::where('id',$request->id)->update(
                ['name' => $request->name]
            );
            return redirect('/category');
        }
    }
}
