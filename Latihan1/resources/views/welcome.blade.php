@extends('layouts.app')

@section('title', 'PBKK')
@section('main')

Nilai anda adalah {{ $nilai }}
@if($nilai>=85 && $nilai<=100)
Grade A
@elseif($nilai>=70 && $nilai<85)
Grade B
@elseif($nilai>=60 && $nilai<70)
Grade C
@elseif($nilai>=50 && $nilai<60)
Grade D
@elseif($nilai>=0 && $nilai<50)
Grade E
@endif

@endsection